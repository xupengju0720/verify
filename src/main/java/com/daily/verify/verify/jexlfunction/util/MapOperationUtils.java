package com.daily.verify.verify.jexlfunction.util;

import java.util.Map;

public class MapOperationUtils {
    public static void put(String type, Object o, Map<String, Object> map) {
        map.put(type, o);
    }

    public static void remove(String type, Map<String, Object> map) {
        map.remove(type);
    }
}
