package com.daily.verify.verify.jexlfunction;

import java.util.*;

import com.daily.verify.verify.jexlfunction.util.MapOperationUtils;
import com.google.gson.Gson;
import org.apache.commons.jexl3.*;

public class JavaScriptEngine {

    public static HashMap<String, Object> exec(HashMap<String, Object> map, String expressString) {
        JexlBuilder jexlBuilder = new JexlBuilder();
        // 创建Jexl表达式引擎
        JexlEngine jexlEngine = jexlBuilder.create();
        // 创建Jexl表达式解析器
        JexlScript jexlScript = jexlEngine.createScript(expressString);
        // 创建Jexl表达式变量上下文
        JexlContext jexlContext = new MapContext();
        jexlContext.set("map", map);
        jexlContext.set("rule", MapOperationUtils.class);
        HashMap execute = (HashMap) jexlScript.execute(jexlContext);
        return execute;
    }

    public static void main(String[] args) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("age", 10);
        map.put("name", "王哥");
        String execpress = "if(map.age == 10){rule.put('type',1,map); rule.remove('name',map);return map;}";
        Gson gson = new Gson();
        System.out.println("转换前格式:" + gson.toJson(map));
        HashMap<String, Object> exec = exec(map, execpress);
        System.out.println("转换后格式:" + gson.toJson(exec));
    }
}