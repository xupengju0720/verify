package com.daily.verify.verify.jesoup.crawler;

/**
 *
 * date 2018/9/26
 * time 13:28
 */
public enum SourceEnums {

    BIQUGE(1, "笔趣阁", "");

    private Integer key;
    private String name;
    private String url;

    SourceEnums(Integer key, String name, String url){
        this.key = key;
        this.name = name;
        this.url = url;
    }

    public Integer getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}
