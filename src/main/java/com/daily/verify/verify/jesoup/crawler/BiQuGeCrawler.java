package com.daily.verify.verify.jesoup.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.IOException;

/**
 * 笔趣阁
 *
 *
 * date 2018/9/25
 * time 17:29
 */
public class BiQuGeCrawler extends AbstractCrawler {
    public static String httpPrefix = "https://www.qbiqu.com";

    @Override
    public String getPage(String url) {
        try {
            //直接拿 html源码
            page = Jsoup.connect(url).get();
            //获取下一页链接
            this.getNext();
            //获取上一页链接
            this.getLast();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //解析出正文返回
        return this.getContent();
    }

    @Override
    protected String getContent() {
        //获取正文内容
        Element cntEl = page.getElementById("content");
        // 八个空格，（制表符号）
        return cntEl.text().replaceAll("        ", "\n\n");
    }

    protected void getNext() {
        //根据jsoup 方法将html语法解析，拿到下一页的链接
        Element ul = page.getElementsByClass("bottem2").get(0).child(4);

        nextUrl = httpPrefix + ul.attr("href");
    }

    protected void getLast() {
        //根据jsoup 方法将html语法解析，拿到上页的链接
        Element ul = page.getElementsByClass("bottem2").get(0).child(2);
        lastUrl = httpPrefix + ul.attr("href");
    }
}
