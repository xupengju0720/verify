package com.daily.verify.verify.jesoup.crawler;

import java.util.Scanner;

/**
 * 逻辑功能控制类
 *
 *
 * date 2018/9/26
 * time 13:11
 */
public class Function {

    // app配置
    private AppConfig config;
    // 爬虫类
    private AbstractCrawler crawler;

    public Function(String firstUrl) {
        crawler = CrawlerFactory.build(config.sourceType);
        startView(firstUrl);
    }

    // 页面浏览
    private void startView(String pageUrl) {
        try {
            //sleep 防止访问过快被封Ip
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (pageUrl.equals(BiQuGeCrawler.httpPrefix + "/71_71111/")) {
            //判断是否爬虫完毕
            System.out.println("爬虫完毕");
        } else {
            String content = crawler.getPage(pageUrl);
            System.out.println("第" + count + "章");
            System.out.println(content);
            //写入文件
            this.exchangeToFile("镇国神婿.txt", content);
        }
    }

    int count = 1;

    private void exchangeToFile(String filename, String context) {
        String title = "第" + count + "章";
        context = "\n\n" + title + "\n\n" + context;
        FileUtils.wFile(filename, context);
        count++;
        startView(crawler.nextUrl);
    }

    // 开始浏览
    private void inputListener() {
        System.out.println("*************");
        System.out.println("* " + config.lastPage + " 上一页   *");
        System.out.println("* " + config.exit + " 退出     *");
        System.out.println("* 其他 下一页 *");
        System.out.println("*************");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        if (config.lastPage.equalsIgnoreCase(input)) {
            // 上一页
            startView(crawler.lastUrl);
        } else if (config.exit.equalsIgnoreCase(input)) {
            // 退出
        } else {
            // 下一页
            startView(crawler.nextUrl);
        }
    }
}
