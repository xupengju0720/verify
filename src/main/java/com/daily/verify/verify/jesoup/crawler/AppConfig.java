package com.daily.verify.verify.jesoup.crawler;

/**
 * 配置文件
 *
 *
 * date 2018/9/26
 * time 11:10
 */
public class AppConfig {

    // 小说源
    public static final Integer sourceType = SourceEnums.BIQUGE.getKey();
    // 退出键
    public static final String exit = "q";
    // 上一页
    public static final String lastPage = "l";

}
