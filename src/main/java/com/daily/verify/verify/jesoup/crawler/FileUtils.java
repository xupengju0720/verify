package com.daily.verify.verify.jesoup.crawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

public class FileUtils {
    //设置写到文件路径
    public static String pathStr = "D:\\download\\git\\txt\\";

    public static boolean wFile(String filename, String context) {
        String filePath = pathStr + "\\" + filename;
        try {
            //构建File对象
            File file = new File(filePath);
            FileOutputStream fos = null;
            //文件已存在
            if (file.exists()) {
                fos = new FileOutputStream(file, true);
                OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
                //写入内容
                osw.write(context);
                //换行
                //linux:\r\n  windows:\n
                osw.write("\r\n");
                osw.flush();
                osw.close();
                return true;
            } else {
                //本地无文件将创建
                file.createNewFile();
                fos = new FileOutputStream(file);
                OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
                osw.write(context);
                //换行
                osw.write("\r\n");
                osw.flush();
                osw.close();
                return true;
            }

        } catch (Exception e) {
            return false;
        }
    }
}